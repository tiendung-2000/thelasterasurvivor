﻿using DG.Tweening;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class EnemyController : MonoBehaviour
{
    public static EnemyController Ins;

    private EnemyMovement enemyMovement;
    private EnemyAttack enemyAttack;

    public SkeletonAnimation Ske;
    [SerializeField] Collider2D col;

    [Header("Variables")]
    public Rigidbody2D theRB;
    public float moveSpeed;

    public int health = 150;

    //public GameObject[] deathSplatters;
    public GameObject hitEffect;

    public bool shouldDropItem;
    public GameObject[] itemsToDrop;
    public float itemDropPercent;

    public bool playerOnZone = false;
    public bool enemyCanMove;
    //public bool enemyCanSpawn;

    private void Awake()
    {
        Ins = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        playerOnZone = false;

        enemyMovement = GetComponent<EnemyMovement>();
        enemyAttack = GetComponent<EnemyAttack>();

        Ske.AnimationState.Complete += AnimationState_Complete;
        if (enemyMovement.isMoving == true)
        {
            Ske.AnimationState.SetAnimation(0, "Move", true);
        }

        //if (enemyAttack.isBurstMode)
        //{
        //    enemyAttack.delayCounter -= Time.deltaTime;
        //}

        if (enemyMovement.isWander)
        {
            enemyMovement.pauseWanderCounter = Random.Range(enemyMovement.pauseLength * .75f, enemyMovement.pauseLength * 1.25f);
        }
    }

    private void AnimationState_Complete(Spine.TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case "Attack":
                if (enemyCanMove == true)
                {
                    enemyMovement.isMoving = true;
                    if (enemyMovement.isMoving == true)
                    {
                        Ske.AnimationState.SetAnimation(0, "Move", true);
                    }
                }
                break;
            case "Die":
                Destroy(gameObject);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerOnZone == true && health > 0 && PlayerController.Ins.currentHealth > 0)
        {
            //if (enemyCanSpawn == true)
            //{
            //    theRB.velocity = enemyMovement.moveDirection * moveSpeed;
            //}

            enemyMovement.EnemyMoving();

            enemyMovement.EnemyWander();

            enemyAttack.EnemyShooting();

            enemyAttack.EnemyAimingSystem();
        }
        else
        {
            theRB.velocity = Vector2.zero;
        }
    }


    public void DamageEnemy(int damage)
    {
        health -= damage;

        //AudioManager.instance.PlaySFX(2);

        SmartPool.Ins.Spawn(hitEffect, transform.position, transform.rotation);

        if (health <= 0)
        {
            col.enabled = false;
            Ske.AnimationState.SetAnimation(0, "Die", false);

            int rotation = Random.Range(0, 4);

            if (shouldDropItem)
            {
                float dropChance = Random.Range(0f, 100f);

                if (dropChance < itemDropPercent)
                {
                    int randomItem = Random.Range(0, itemsToDrop.Length);

                    SmartPool.Ins.Spawn(itemsToDrop[randomItem], transform.position, transform.rotation);
                }
            }
        }
    }
}
