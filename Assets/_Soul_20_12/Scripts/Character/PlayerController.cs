﻿using API.UI;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Ins;

    public int currentHealth;

    public int curPlayerMaxHP;

    private Gun gun;

    public float moveSpeed;
    public Vector2 moveInput;

    public Collider2D col;

    public Rigidbody2D theRB;

    public Transform gunArm;

    public SkeletonAnimation skeletonAnimation;
    public string currentState;
    public AnimationReferenceAsset idle, move, skill, die;
    public string currentAnimation;

    
    public bool canMove = true;

    public List<Gun> availableGuns = new List<Gun>();
    public List<Gun> availableDupliGuns = new List<Gun>();
    [HideInInspector]
    public int currentGun;

    public GameObject theHand;

    public Vector3 theGun;

    public ParticleSystem shieldBuffFX, shiedBreakFX, healthBuffFX, coinCollectFX;

    private void Awake()
    {
        Ins = this;
    }

    void Start()
    {
        SetCharacterState(currentState);

        OnHPUpgrade(DynamicDataManager.Ins.CurPlayerHPUpgrade);
        OnSpeedUpgrade(DynamicDataManager.Ins.CurPlayerSpeedUpgrade);
    }

    private void OnEnable()
    {
        canMove = true;

        currentState = "Idle";
        if (col.enabled == false)
        {
            col.enabled = true;
        }
    }

    void Update()
    {
        if (currentHealth > 0)
        {
            PlayerMove();
        }
        else
        {
            theRB.velocity = Vector2.zero;
        }

        DynamicDataManager.Ins.damagePlayerCooldown -= Time.deltaTime;

        if (DynamicDataManager.Ins.IsStayDmg == true && DynamicDataManager.Ins.damagePlayerCooldown < 0f)
        {
            DynamicDataManager.Ins.damagePlayerCooldown = DynamicDataManager.Ins.DamagePlayerCooldownMaxValue;
            DataManager.Ins.DamagePlayer();
        }
    }

    public void PlayerMove()
    {
        theRB.velocity = moveInput * PlayerSkillManager.instance.activeMoveSpeed;

        if (canMove || !canMove)
        {
            moveInput.x = UltimateJoystick.GetHorizontalAxis("Player Movement JoyStick");
            moveInput.y = UltimateJoystick.GetVerticalAxis("Player Movement JoyStick");
            moveInput.Normalize();

        }
        else
        {
            theRB.velocity = Vector2.zero;
            SetCharacterState("Idle");
        }

        if (moveInput != Vector2.zero && canMove && currentHealth > 0)
        {
            SetCharacterState("Move");
        }
        else
        {
            if (canMove && currentHealth > 0)
            {
                SetCharacterState("Idle");
            }
        }
    }

    public void SetAnimation(AnimationReferenceAsset animation, bool loop, float timescale)
    {
        if (animation.name.Equals(currentAnimation))
        {
            return;
        }
        skeletonAnimation.state.SetAnimation(0, animation, loop).TimeScale = timescale;
        currentAnimation = animation.name;
    }

    public void SetCharacterState(string state)
    {
        if (state.Equals("Idle"))
        {
            currentState = "Idle";
            SetAnimation(idle, true, 1f);
        }
        else if (state.Equals("Move"))
        {
            currentState = "Move";
            SetAnimation(move, true, 1f);
        }
        else if (state.Equals("Skill"))
        {
            currentState = "Skill";
            SetAnimation(skill, true, 1f);
        }
        else if (state.Equals("Die"))
        {
            currentState = "Die";
            SetAnimation(die, false, 1f);
        }
    }

    public void SwitchGun()
    {
        foreach (Gun theGun in availableGuns)
        {
            theGun.gameObject.SetActive(false);
        }
        foreach (Gun theGun in availableDupliGuns)
        {
            theGun.gameObject.SetActive(false);
        }

        availableGuns[currentGun].gameObject.SetActive(true);
    }
    private void OnHPUpgrade(int level)
    {
        int numHP = ResourceSystem.Ins.CharactersDatabase.Characters[DynamicDataManager.Ins.CurPlayer].Data.HP[level];
        PlayerHub.Ins.OnHealthChange(numHP);
        currentHealth = numHP;
        curPlayerMaxHP = numHP;
    }

    private void OnSpeedUpgrade(int level)
    {
        int numSpeed = ResourceSystem.Ins.CharactersDatabase.Characters[DynamicDataManager.Ins.CurPlayer].Data.Speed[level];
        moveSpeed = numSpeed;
    }
}
