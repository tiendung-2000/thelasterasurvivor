﻿using API.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{

    #region PlayerHealthController

    public void DamagePlayer()
    {
        PlayerController.Ins.currentHealth--;
        DynamicDataManager.Ins.OnHealthChange?.Invoke(PlayerController.Ins.currentHealth);
        if (PlayerController.Ins.currentHealth <= 0)
        {
            PlayerController.Ins.col.enabled = false;
            PlayerController.Ins.canMove = false;
            StartCoroutine(DelayDead());
            PlayerController.Ins.SetCharacterState("Die");
        }
    }
    IEnumerator DelayDead()
    {
        yield return new WaitForSeconds(1f);

        PlayerController.Ins.gameObject.SetActive(false);
        CanvasManager.Ins.OpenUI(UIName.RevivePopup, null);
    }
    public void HealPlayer(int healAmount)
    {
        int curPlayerMaxHP = ResourceSystem.Ins.CharactersDatabase.Characters[DynamicDataManager.Ins.CurPlayer].Data.HP[DynamicDataManager.Ins.CurPlayerHPUpgrade];
        PlayerController.Ins.currentHealth += healAmount;
        if (PlayerController.Ins.currentHealth > curPlayerMaxHP)
        {
            PlayerController.Ins.currentHealth = curPlayerMaxHP;
        }
        PlayerHub.Ins.OnHealthChange(PlayerController.Ins.currentHealth);
    }

    #endregion
}
