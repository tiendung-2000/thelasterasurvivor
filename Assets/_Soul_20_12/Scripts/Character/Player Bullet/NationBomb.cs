using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NationBomb : MonoBehaviour
{
    public int damageToGive = 50;

    private void OnEnable()
    {
        CinemachineShake.Instance.ShakeCamera(3f, .5f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<EnemyController>().DamageEnemy(damageToGive);
        }

        if (other.tag == "Boss")
        {
            BossController.Ins.TakeDamage(damageToGive);

            SmartPool.Ins.Spawn(BossController.Ins.hitEffect, transform.position, transform.rotation);
        }

        DOVirtual.DelayedCall(1f, () =>
        {
            SmartPool.Ins.Despawn(gameObject);
        });
    }
}
