using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RocketBullet : MonoBehaviour
{
    public GameObject explodeEffect;

    public int damageToGive;

    public TrailRenderer trail;

    private void OnDisable()
    {
        trail.Clear();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //DOVirtual.DelayedCall(.5f, () =>
        //{
        //    SmartPool.Ins.Spawn(explodeEffect, transform.position, transform.rotation);
        //});
        if (other.tag == "Block")
        {
            SmartPool.Ins.Spawn(explodeEffect, transform.position, transform.rotation);
            SmartPool.Ins.Despawn(gameObject);
        }

        if (other.tag == "Enemy")
        {
            other.GetComponent<EnemyController>().DamageEnemy(damageToGive);
            SmartPool.Ins.Spawn(explodeEffect, transform.position, transform.rotation);
            SmartPool.Ins.Despawn(gameObject);
        }

        if (other.tag == "Boss")
        {
            BossController.Ins.TakeDamage(damageToGive);

            SmartPool.Ins.Spawn(BossController.Ins.hitEffect, transform.position, transform.rotation);
            SmartPool.Ins.Despawn(gameObject);
        }

        //AudioManager.instance.PlaySFX(4);
    }
    //private void OnBecameInvisible()
    //{
    //    SmartPool.Ins.Despawn(gameObject);
    //}
}
