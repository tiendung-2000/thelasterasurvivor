using API.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonControllerUI : BaseUIMenu
{
    public static ButtonControllerUI instance;

    public bool pointerDown;

    [SerializeField] Button shootButton, buyButton, skillButton, switchButton;

    public bool buy = false;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        shootButton.onClick.AddListener(OnShoot);
        buyButton.onClick.AddListener(OnBuy);
        skillButton.onClick.AddListener(OnSkill);
        switchButton.onClick.AddListener(OnSwitch);
    }

    private void OnEnable()
    {
        Time.timeScale = 1f;

    }

    private void FixedUpdate()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            OnSkill();
        }
    }
    public void OnPointerDown()
    {
        pointerDown = true;
    }

    public void OnPointerUp()
    {
        pointerDown = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (pointerDown)
        {
            OnShoot();
        }
    }

    public void OnSwitch()
    {
        if (PlayerController.Ins.availableGuns.Count > 0)
        {
            PlayerController.Ins.currentGun++;
            if (PlayerController.Ins.currentGun >= PlayerController.Ins.availableGuns.Count)
            {
                PlayerController.Ins.currentGun = 0;
            }

            PlayerController.Ins.SwitchGun();

        }
        else
        {
            Debug.LogError("Player has no guns!");
        }
    }

    public void OnShoot()
    {
        PlayerController.Ins.availableGuns[PlayerController.Ins.currentGun].GunFire();
        if (PlayerSkillManager.instance.dualChecker == true)
            PlayerController.Ins.availableDupliGuns[PlayerController.Ins.currentGun].GunFire();
    }

    public void OnSkill()
    {
        switch (PlayerSkillManager.instance.playerID)
        {
            case 0:
                PlayerSkillManager.instance.Dash();
                break;
            case 1:
                PlayerSkillManager.instance.DualGun();
                break;
            case 2:
                PlayerSkillManager.instance.HolyShield();
                break;
            case 3:
                PlayerSkillManager.instance.BulletGrenade();
                break;
            case 4:
                PlayerSkillManager.instance.Speed();
                break;
            default:
                print("NIg");
                break;
        }
    }

    public void OnBuy()
    {
        Debug.Log("isBuy");
        if (RoomCenter.Ins.typeItem == 1)
        {
            buy = true;
        }
        else if (RoomCenter.Ins.typeItem == 2)
        {
            buy = true;
        }
        else if (RoomCenter.Ins.typeItem == 3)
        {
            buy = true;
        }
    }
}
