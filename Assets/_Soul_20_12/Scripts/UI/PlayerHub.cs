using API.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHub : MonoBehaviour
{
    public static PlayerHub Ins;

    public Text healthText;
    public Text coinText;
    public int coinCollect;
    public Image healthSlider;

    //coin thu thap trong level se la 1 bien rieng va sau khi end level se + voi coin trong data

    private void Awake()
    {
        Ins = this;
    }

    void Start()
    {
        OnHealthChange(PlayerController.Ins.currentHealth);
        DynamicDataManager.Ins.OnHealthChange += OnHealthChange;
    }

    private void Update()
    {
        CoinInLevel();
    }

    public void OnHealthChange(int health)
    {
        int curPlayerMaxHP = ResourceSystem.Ins.CharactersDatabase.Characters[DynamicDataManager.Ins.CurPlayer].Data.HP[DynamicDataManager.Ins.CurPlayerHPUpgrade];
        healthSlider.fillAmount = (float)health / curPlayerMaxHP;
        healthText.text = health.ToString() + "/" + curPlayerMaxHP;
    }

    public void CoinInLevel()
    {
        coinText.text = coinCollect.ToString();
    }
}
