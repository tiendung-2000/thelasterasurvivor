using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIName
{
    //UI
    public static string SelectLevelUI = "SelectLevelUI";
    public static string RewardsUI = "RewardsUI";
    public static string SelectCharactersUI = "SelectCharactersUI";
    public static string CompleteUI = "CompleteUI";
    public static string CoinBar = "CoinBar";

    //========================================================================//

    //GameplayUI
    public static string GameplayUI = "GameplayUI";
    public static string PauseSettingUI = "PauseSettingUI";
    public static string PlayerHubUI = "PlayerHubUI";
    public static string BossHubUI = "BossHubUI";
    public static string LoadingUI = "LoadingUI";
    public static string ButtonControllerUI = "ButtonControllerUI";
    public static string MiniMapUI = "MiniMapUI";

    //========================================================================//

    //Popup
    public static string AdsToCoinPopup = "AdsToCoinPopup";
    public static string WeaponPopup = "WeaponPopup";
    public static string RevivePopup = "RevivePopup";
}
