using API.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevelUI : BaseUIMenu
{
    [SerializeField] Button startButton;
    [SerializeField] GameObject start;

    [Header("===LevelUI Button===")]
    [SerializeField] Button selectLevelButton;
    [SerializeField] Button settingButton;
    [SerializeField] Button rewardButton;

    [SerializeField] GameObject settingPopup;
    [SerializeField] bool isOpenSettingPopup;

    [SerializeField] TMP_Text levelName;

    private void Start()
    {
        startButton.onClick.AddListener(OnStart);

        isOpenSettingPopup = true;

        selectLevelButton.onClick.AddListener(OnClickSelectLevelButton);

        settingButton.onClick.AddListener(OnClickSettingButton);
        rewardButton.onClick.AddListener(OnClickRewardButton);
        //levelName.text = ResourceSystem.Ins.levels[]
    }

    private void OnEnable()
    {
        Time.timeScale = 1f;
    }

    public void OnStart()
    {
        UITransition.Ins.ShowTransition(() =>
        {
            start.gameObject.SetActive(false);
        });
    }

    public void OnClickSelectLevelButton()
    {
        //SFX Here

        OnSelectLevel();
    }

    public void OnClickSettingButton()
    {
        //SFX Here

        OnSetting();
    }

    public void OnClickRewardButton()
    {
        //SFX Here

        OnReward();
    }

    public void OnSelectLevel()
    {
        CanvasManager.Ins.OpenUI(UIName.SelectCharactersUI, null);
        CanvasManager.Ins.OpenUI(UIName.CoinBar, null);
        Close();
    }

    public void OnSetting()
    {
        //if (isOpenSettingPopup)
        //{
        //    settingPopup.SetActive(true);
        //    isOpenSettingPopup = false;
        //}
        //else
        //{
        //    settingPopup.SetActive(false);
        //    isOpenSettingPopup = true;
        //}

        Debug.Log("Click");

        settingPopup.SetActive(isOpenSettingPopup == true ? true : false);
        isOpenSettingPopup = isOpenSettingPopup == false ? true : false;
    }

    public void OnReward()
    {
        CanvasManager.Ins.OpenUI(UIName.RewardsUI, null);
        CanvasManager.Ins.OpenUI(UIName.CoinBar, null);
        Close();
    }
}
