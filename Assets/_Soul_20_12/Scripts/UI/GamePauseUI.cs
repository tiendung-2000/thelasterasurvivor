using API.UI;
using DG.Tweening;
using DG.Tweening.Plugins.Options;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GamePauseUI : BaseUIMenu
{
    [SerializeField] Button closeButton;
    [SerializeField] Button homeButton;
    [SerializeField] Button replayButton;

    private void Start()
    {
        homeButton.onClick.AddListener(OnClickHomeButton);
        replayButton.onClick.AddListener(OnClickReplayButton);
        closeButton.onClick.AddListener(OnClickCloseButton);

        CanvasManager.Ins.CloseUI(UIName.GameplayUI);
    }

    private void OnEnable()
    {
        Time.timeScale = 0f;
    }

    private void OnDisable()
    {
        Time.timeScale = 1f;
    }

    private void OnClickCloseButton()
    {
        OnClose();
    }

    private void OnClose()
    {
        CanvasManager.Ins.OpenUI(UIName.GameplayUI, null);
        Close();
    }

    private void OnClickHomeButton()
    {
        OnHome();
        CinemachineShake.Instance.ResetShakeCamera();
    }

    void OnHome()
    {
        //SmartPool.Ins.Despawn(ResourceSystem.Ins.CurLevelGameObj);
        //CanvasManager.Ins.OpenUI(UIName.SelectLevelUI, null);
        //Close();


        //PlayerController.Ins.currentHealth = PlayerController.Ins.curPlayerMaxHP;
        ResetPlayerPos();
        DynamicDataManager.Ins.OnHealthChange?.Invoke(PlayerController.Ins.curPlayerMaxHP);
        SmartPool.Ins.Despawn(ResourceSystem.Ins.CurLevelGameObj);
        ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].gameObject.SetActive(false);
        CanvasManager.Ins.OpenUI(UIName.SelectLevelUI, null);
        Close();
    }

    private void OnClickReplayButton()
    {
        OnReplay();
        CinemachineShake.Instance.ResetShakeCamera();
    }

    void OnReplay()
    {
        CanvasManager.Ins.OpenUI(UIName.LoadingUI, null);

        int openPopup = 5;
        if (Random.Range(1, 10) < openPopup)
        {
            CanvasManager.Ins.OpenUI(UIName.WeaponPopup, null);
        }

        ResetPlayerPos();
        SmartPool.Ins.Despawn(ResourceSystem.Ins.CurLevelGameObj);
        ResourceSystem.Ins.SpawnLevel(1);
        CanvasManager.Ins.OpenUI(UIName.GameplayUI, null);
        Close();
    }

    void ResetPlayerPos()
    {
        ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].transform.position = Vector3.zero;
    }
}
