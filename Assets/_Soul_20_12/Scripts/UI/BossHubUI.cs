using API.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHubUI : BaseUIMenu
{
    public static BossHubUI Ins;

    public Slider bossHealthBar;
    void Start()
    {
        BossController.Ins.bossHubUI = this;
        bossHealthBar.maxValue = BossController.Ins.currentHealth;
        bossHealthBar.value = BossController.Ins.currentHealth;
    }
}
