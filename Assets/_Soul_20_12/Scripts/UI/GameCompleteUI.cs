using API.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class GameCompleteUI : BaseUIMenu
{
    [SerializeField] Button homeButton;
    [SerializeField] Button replayButton;
    [SerializeField] Button nextButton;

    [SerializeField] Text coinBonusText;
    [SerializeField] Text coinCollectText;
    [SerializeField] Text coinTotalText;

    int coinBonus;
    int coinCollect;
    int coinTotal;

    int coinDefault = 0;

    [SerializeField] Button watchAdsButton;

    private void Start()
    {
        homeButton.onClick.AddListener(OnClickHomeButton);
        replayButton.onClick.AddListener(OnClickReplayButton);
        nextButton.onClick.AddListener(OnClickNextButton);

        watchAdsButton.onClick.AddListener(OnClickWatchAdsButton);

        Sequence getCoin = DOTween.Sequence();

        getCoin.AppendCallback(() =>
        {
            coinBonus = LevelGate.Ins.coinBonus;
            coinCollect = PlayerHub.Ins.coinCollect;
            coinTotal = (LevelGate.Ins.coinBonus + PlayerHub.Ins.coinCollect);
        }).OnComplete(() =>
        {
            ShowCoin();
        });
    }

    public void ShowCoin()
    {
        PlayChangeGoldEffect(coinTotalText);
        coinBonusText.text = coinBonus.ToString();
        coinCollectText.text = coinCollect.ToString();
        coinTotalText.text = coinTotal.ToString();
    }

    public void OnClickHomeButton()
    {
        OnHome();
    }

    public void OnClickReplayButton()
    {
        OnReplay();
    }

    public void OnClickNextButton()
    {
        OnNext();
    }

    public void OnClickWatchAdsButton()
    {
        OnClaimX2();
    }

    void OnHome()
    {
        CanvasManager.Ins.OpenUI(UIName.SelectLevelUI, null);
        Close();
    }

    void OnReplay()
    {
        Close();

        Sequence loading = DOTween.Sequence();
        loading.AppendCallback(() =>
        {
            CanvasManager.Ins.OpenUI(UIName.LoadingUI, null);

        }).OnComplete(() =>
        {
            CanvasManager.Ins.OpenUI(UIName.GameplayUI, null);
            ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].gameObject.SetActive(true);
            ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].transform.position = Vector3.zero;


            ResourceSystem.Ins.SpawnLevel(1);

            CharacterSelectManager.Ins.activePlayer = ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer];
            CameraController.Ins.cameraMovement = ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].gameObject.transform;
        });
    }

    void OnNext()
    {
        Close();

        Sequence loading = DOTween.Sequence();
        loading.AppendCallback(() =>
        {
            CanvasManager.Ins.OpenUI(UIName.LoadingUI, null);

        }).OnComplete(() =>
        {
            CanvasManager.Ins.OpenUI(UIName.GameplayUI, null);
            ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].gameObject.SetActive(true);
            ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].transform.position = Vector3.zero;

            ResourceSystem.Ins.SpawnLevel(0);

            CharacterSelectManager.Ins.activePlayer = ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer];
            CameraController.Ins.cameraMovement = ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].gameObject.transform;
        });
    }

    void OnClaimX2()
    {

    }

    public void PlayChangeGoldEffect(Text txtGold, System.Action callback = null)
    {
        IEnumerator IPlayChangeGoldEffect()
        {
            var goldAfter = coinTotal;
            var goldBefore = coinDefault;
            bool increase = goldAfter > goldBefore;
            float goldBf = goldBefore;
            var distance = increase ? goldAfter - goldBefore : goldBefore - goldAfter;
            var perFrame = distance * Time.deltaTime / 2f;
            while (increase ? goldBf < goldAfter : goldAfter < goldBf)
            {
                if (increase)
                    goldBf += perFrame;
                else
                    goldBf -= perFrame;
                int goldShow = (int)goldBf;
                txtGold.text = goldShow.ToString();
                yield return null;
            }
            txtGold.text = goldAfter.ToString();
            callback?.Invoke();
        }
        StartCoroutine(IPlayChangeGoldEffect());
    }
}
