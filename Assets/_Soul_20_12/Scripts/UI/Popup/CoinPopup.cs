using API.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CoinPopup : BaseUIMenu
{
    [SerializeField] Button closeButton;
    [SerializeField] Button watchAdsButton;

    void Start()
    {
        closeButton.onClick.AddListener(OnClickCloseButton);
        watchAdsButton.onClick.AddListener(OnClickWatchAdsButton);
    }

    public void OnClickWatchAdsButton()
    {
        OnWatchAds();
    }

    public void OnClickCloseButton()
    {
        OnClose();
    }

    public void OnWatchAds()
    {
        DynamicDataManager.Ins.CurNumCoin += 1000;
    }

    public void OnClose()
    {
        Close();
    }

   

    //public void PlayChangeGoldEffect(TextMeshProUGUI txtGold, System.Action callback = null)
    //{
    //    IEnumerator IPlayChangeGoldEffect()
    //    {
    //        var gold = DataController.Instance.Gold;
    //        var goldBefore = DataController.Instance.GoldBefore;
    //        bool increase = gold > goldBefore;
    //        float goldBf = goldBefore;
    //        var distance = increase ? gold - goldBefore : goldBefore - gold;
    //        var perFrame = distance * Time.deltaTime / .5f;
    //        while (increase ? goldBf < gold : gold < goldBf)
    //        {
    //            if (increase)
    //                goldBf += perFrame;
    //            else
    //                goldBf -= perFrame;
    //            int goldShow = (int)goldBf;
    //            txtGold.text = goldShow.ToString();
    //            yield return null;
    //        }
    //        txtGold.text = gold.ToString();
    //        callback?.Invoke();
    //    }
    //    StartCoroutine(IPlayChangeGoldEffect());
    //}
}
