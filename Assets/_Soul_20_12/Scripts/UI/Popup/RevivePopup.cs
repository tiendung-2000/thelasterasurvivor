using API.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RevivePopup : BaseUIMenu
{
    [SerializeField] Button reviveButton;
    [SerializeField] Button noButton;
    [SerializeField] Button exitButton;

    [SerializeField] float timeCountDown;

    [SerializeField] Text countDownText;

    [SerializeField] int reviveCount = 1;

    [SerializeField] GameObject counter;

    Tween OnEnableTween;

    private void Start()
    {
        reviveButton.onClick.AddListener(OnClickReviveButton);
        noButton.onClick.AddListener(OnClickNoButton);
        exitButton.onClick.AddListener(OnClickExitButton);
    }

    void OnEnable()
    {
        if (reviveCount < 1)
        {
            counter.SetActive(true);
            noButton.gameObject.SetActive(true);
            exitButton.gameObject.SetActive(false);

            CanvasManager.Ins.CloseUI(UIName.GameplayUI);

            OnEnableTween?.Kill();

            OnEnableTween = DOVirtual.Float(timeCountDown, 0, timeCountDown, (value) =>
            {
                countDownText.text = Mathf.Round(value).ToString();
            }).SetEase(Ease.Linear).OnComplete(() =>
            {
                exitButton.gameObject.SetActive(true);
                noButton.gameObject.SetActive(false);
                reviveButton.gameObject.SetActive(false);
            });
        }
        else
        {
            noButton.gameObject.SetActive(false);
            exitButton.gameObject.SetActive(true);
            counter.SetActive(false);
        }
    }

    void OnClickReviveButton()
    {
        OnRevive();
    }

    void OnRevive()
    {
        PlayerController.Ins.currentHealth = PlayerController.Ins.curPlayerMaxHP;
        DynamicDataManager.Ins.OnHealthChange?.Invoke(PlayerController.Ins.curPlayerMaxHP);
        ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].gameObject.SetActive(true);
        Close();
        CanvasManager.Ins.OpenUI(UIName.GameplayUI, null);
    }

    void OnClickNoButton()
    {
        OnNo();
        CinemachineShake.Instance.ResetShakeCamera();
    }

    void OnClickExitButton()
    {
        OnNo();
        CinemachineShake.Instance.ResetShakeCamera();
    }

    void OnNo()
    {
        PlayerController.Ins.currentHealth = PlayerController.Ins.curPlayerMaxHP;
        DynamicDataManager.Ins.OnHealthChange?.Invoke(PlayerController.Ins.curPlayerMaxHP);
        SmartPool.Ins.Despawn(ResourceSystem.Ins.CurLevelGameObj);
        ResourceSystem.Ins.players[DynamicDataManager.Ins.CurPlayer].transform.position = Vector3.zero;
        CanvasManager.Ins.OpenUI(UIName.SelectLevelUI, null);
        Close();
    }
}
