using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
//[RequireComponent(typeof(Button))]
public class RewardTabs : MonoBehaviour
{
    public int index;

    public Button claimButton;
    public Button lockClaim;

    [SerializeField] Text rewardName;
    [SerializeField] Text coinTextValue;

    public Image tabBackground;
    public Sprite lockSp;
    public Sprite unlockSp;

    [SerializeField] GameObject collected;

    RewardTabs currentTab;

    private void Start()
    {
        claimButton.onClick.AddListener(OnClickClaimButton);
    }

    public void SetUp(string name, string value)
    {
        rewardName.text = name;
        coinTextValue.text = value;      
    }

    void OnClickClaimButton()
    {
        OnClaim();
    }

    void OnClaim()
    {
        //Coin Reward
        //Debug.Log(ResourceSystem.Ins.RewardsLevel.RewardsData[index].RewardValue);

        //Coin Update

        DynamicDataManager.Ins.CurNumCoin += ResourceSystem.Ins.RewardsLevel.RewardsData[index].RewardValue;

        claimButton.gameObject.SetActive(false);
        collected.gameObject.SetActive(true);
    }
}
