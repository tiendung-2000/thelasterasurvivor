using API.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardUI : BaseUIMenu
{
    [SerializeField] Button backButton;
    //[SerializeField] Button addCoinButton;

    [SerializeField] Transform tabsParent;
    [SerializeField] RewardTabs rewardTab;

    private void Start()
    {
        backButton.onClick.AddListener(OnClickBackButton);

        SetUpReward();
    }

    void SetUpReward()
    {
        var rewardLevel = ResourceSystem.Ins.RewardsLevel;
        var index = 0;
        foreach (var reward in rewardLevel.RewardsData)
        {

            if (reward.isUnlock == true)
            {
                rewardTab.tabBackground.sprite = rewardTab.unlockSp;
                rewardTab.claimButton.gameObject.SetActive(true);
                rewardTab.lockClaim.gameObject.SetActive(false);
            }
            else
            {
                rewardTab.tabBackground.sprite = rewardTab.lockSp;
                rewardTab.claimButton.gameObject.SetActive(false);
                rewardTab.lockClaim.gameObject.SetActive(true);
            }
            var tab = Instantiate(rewardTab, tabsParent);
            tab.index = index;
            tab.SetUp(reward.RewardName, reward.RewardValue.ToString());
            index++;
        }
    }

    public void OnClickBackButton()
    {
        OnBack();
    }

    public void OnBack()
    {
        CanvasManager.Ins.OpenUI(UIName.SelectLevelUI, null);
        CanvasManager.Ins.CloseUI(UIName.CoinBar);
        Close();
    }
}
