using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Scriptable Level Reward")]
public class ScriptableRewardsLevel : ScriptableObject {

    //public GameObject Prefab;

    public int TotalReward;

    public int[] Level;

    //public int[] Rewards;

    public List<RewardData> RewardsData;

}

[System.Serializable]
public class RewardData
{
    public bool isUnlock;
    public string RewardName;

    public int RewardValue;

    //public Image RewardImage;
}