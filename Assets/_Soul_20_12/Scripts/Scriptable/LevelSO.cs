using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Scriptable Level Generator")]
public class LevelSO : ScriptableObject
{
    public GameObject levelPrefab;
    public string levelName;
}
