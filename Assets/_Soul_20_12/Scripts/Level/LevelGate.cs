using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using API.UI;

public class LevelGate : MonoBehaviour
{
    public static LevelGate Ins;

    [SerializeField] GameObject gate_1;
    [SerializeField] GameObject gate_2;

    public int coinBonus = 1000;

    private void Awake()
    {
        Ins = this;
    }
    void Start()
    {
        gate_1.transform.DORotate(new Vector3(0f, 0f, 360f), 5f, RotateMode.Fast)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Restart)
            .SetRelative();
        gate_2.transform.DORotate(new Vector3(0f, 0f, -360f), 2f, RotateMode.Fast)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Restart)
            .SetRelative();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            CanvasManager.Ins.OpenUI(UIName.CompleteUI, null);
        }
    }
}
