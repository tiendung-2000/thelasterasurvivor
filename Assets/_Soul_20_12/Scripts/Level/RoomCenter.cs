﻿using API.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCenter : MonoBehaviour
{
    public static RoomCenter Ins;

    //public ShopItem[] shopItem;

    public int typeItem;

    public bool openWhenEnemiesCleared;

    public List<EnemyController> enemies;
    public List<BossController> bosses;

    public GameObject enemyGroup;

    [SerializeField] Collider2D colCheckPlayer;

    public Room theRoom;

    public SpriteRenderer enemyRoomIcon;

    [SerializeField] bool isEnemyCenter;
    public bool isBossCenter;
    public GameObject theBoss;

    public Transform checkPoint;
    public int maxX;
    public int minX;
    public int maxY;
    public int minY;

    // Start is called before the first frame update

    private void Awake()
    {
        Ins = this;
    }
    void Start()
    {
        if (isEnemyCenter)
        {
            EnemyController[] enemyGr = enemyGroup.GetComponentsInChildren<EnemyController>();

            foreach (EnemyController enemy in enemyGr)
            {
                enemies.Add(enemy);

                enemy.gameObject.SetActive(false);
            }
        }

        if (openWhenEnemiesCleared)
        {
            theRoom.closeWhenEntered = true;
        }
    }

    void Update()
    {
        if (enemies.Count > 0 && theRoom.roomActive && openWhenEnemiesCleared)
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i] == null)
                {
                    enemies.RemoveAt(i);

                    i--;
                }
            }

            if (enemies.Count == 0)
            {
                theRoom.OpenDoors();
                enemyRoomIcon.enabled = false;
            }
        }

        if (bosses.Count > 0 && theRoom.roomActive && openWhenEnemiesCleared)
        {
            for (int i = 0; i < bosses.Count; i++)
            {
                if (bosses[i] == null)
                {
                    bosses.RemoveAt(i);
                    i--;
                }
            }

            if (bosses.Count == 0)
            {
                theRoom.OpenDoors();
            }
        }
    }

    public void GetTarget()
    {
        if (checkPoint != null)
        {
            checkPoint.localPosition = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 0f);
            Debug.Log(checkPoint.localPosition);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (isBossCenter)
            {
                CanvasManager.Ins.OpenUI(UIName.BossHubUI, null);

                theBoss.SetActive(true);
            }
            if (isEnemyCenter)
            {
                for (int i = 0; i < enemies.Count; i++)
                {
                    //Debug.Log("PlayerInRoom");
                    enemies[i].gameObject.SetActive(true);
                }
            }
        }
    }
}
