﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{
    public int coinValue = 1;

    public float waitToBeCollected;

    // Update is called once per frame
    void Update()
    {
        if (waitToBeCollected > 0)
        {
            waitToBeCollected -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && waitToBeCollected <= 0)
        {
            PlayerController.Ins.coinCollectFX.Play();

            PlayerHub.Ins.coinCollect += coinValue;

            SmartPool.Ins.Despawn(gameObject);

            //AudioManager.instance.PlaySFX(5);
        }
    }
}
